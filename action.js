const DATA = [
    {
        id:34,
        name:'Ваня Иванов',
        age:30,
        avatar:'./images.jpg'
    },
    {
        id:1,
        name:'Сева Репин',
        age:18,
        avatar:'./i1.jpg'
    },
    {
        id:67,
        name:'Аня Клон',
        age:17,
        avatar:'./i2.jpg'
    },
    {
        id:345,
        name:'Ваня Саша',
        age:38,
        avatar:'./i3.jpg'
    }
];
const container = document.getElementsByClassName('container')[0];//сделали глобальной для последующего использования и очистки в renderAll
const addButton = document.getElementById('add');
addButton.addEventListener('click',addUser);


function renderUser(user){
  
   container.innerHTML+=`
   <div class="user">
       <img src="${user.avatar}" class="user-logo" alt="">
       <div class="user-info">
          <span class="user-name">${user.name}</span>
         <span class="user-age"> Возраст ${user.age} лет</span>
        </div>
        <div class="close" onclick="deleteUser(${user.id})">X</div>
   </div>
 
   `;
}

function renderAll(){
    container.innerHTML = '';//почистить контейнер от предыдущего использования
     DATA.forEach(user => {// сокращенная функция for
        renderUser(user);

    } );
}
// удаление карточки (удаление элемента массива)
function deleteUser(id){
    // console.log(id);
         const user = DATA.find(item => item.id ==id);// item создана на лету проверка  и нахождение по идексу элемента
  //  console.log(user);
const index = DATA.indexOf(user);
// console.log(index);
DATA.splice(index,1);
console.log(DATA);
renderAll();
}

//добавление  карточки ввод имя возраст


function addUser(){
    const name = document.getElementById('name');
    const age = document.getElementById('age');
    if(name.value.length > 0 && age.value.length >= 0){
       if( isNaN(+age.value)  || +age.value <=0 ){//проверка isNaN число или нет
            alert('Введите корректный возраст');
        }else{
            const newUser ={
                id: getMaxId() +1,
                name: name.value,
                age: age.value,
                avatar:'./n.jpg'
            };
            DATA.push(newUser);
            name.value ='';
            age.value ='';
            renderAll();

        }
    }
    else{
        alert('Заполните поля');
    }

}

function getMaxId(){
   var maxId =0;
   DATA.forEach(user => {
       if(maxId < user.id){
           maxId = user.id;
       }
   });
   return maxId;
}
console.log(getMaxId);
renderAll();

